---
title: "Exemple de publication"
date: 2023-03-21
draft: false
---

Un modèle de site internet pour les sections de l'[Union syndicale Solidaires](https://solidaires.org/) (basé sur [Hugo](https://gohugo.io/))

Ceci est la 1ère publication à titre d'exemple.

Pour plus d'information consultez le [`README`](https://framagit.org/hugolidaires/site-template/-/blob/stable/README.md) du projet.
