`Site de la section Solidaires Red Hat`
======================================

Le site internet de la section syndicale _Solidaires Informatique Red Hat_.

Il utilise le modèle de site internet pour les sections [Solidaires Informatique](https://solidairesinformatique.org/) [`hugolidaires`](https://framagit.org/hugolidaires/site-template/-/tree/sol_info) (basé sur [Hugo](https://gohugo.io/)).